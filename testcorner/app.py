from flask import Flask, render_template
import xml.etree.ElementTree as ET
import requests
import os
from util import hashFile, getFileSize

app = Flask(__name__)

@app.route('/')
def index():
    return "Hello world!"

@app.route('/testsearch')
def toSearch():
  token = xml()
  test = 'C:/Users/hrothgar32/Downloads/Harry_Potter_and_the_Chamber_of_Secrets_2002.mkv'
  testHash = hashFile(test)
  testSize = getFileSize(test)
  print(testSize)
  tree = ET.fromstring('<methodCall></methodCall>')
  name = ET.SubElement(tree, 'methodName')
  name.text = 'SearchSubtitles'
  params = ET.SubElement(tree, 'params')
  tokenParam = ET.SubElement(params, 'param')
  tokenVal = ET.SubElement(tokenParam, 'value')
  tokenString = ET.SubElement(tokenVal, 'string')
  tokenString.text = token
  dataParam =  ET.SubElement(params, 'param')
  dataParamVal = ET.SubElement(dataParam, 'value')
  array = ET.SubElement(dataParamVal, 'array')
  data = ET.SubElement(array, 'data')
  dataValue = ET.SubElement(data, 'value')
  struct = ET.SubElement(dataValue, 'struct')
  languageMember = ET.SubElement(struct, 'member')
  languageMemberName = ET.SubElement(languageMember, 'name')
  languageMemberName.text = 'sublanguageid'
  languageMemberVal = ET.SubElement(languageMember, 'value')
  languageMemberValString = ET.SubElement(languageMemberVal, 'string')
  languageMemberValString.text = 'cze, eng, ger, slo'
  movieHashMember = ET.SubElement(struct, 'member')
  movieHashMemberName = ET.SubElement(movieHashMember, 'name')
  movieHashMemberName.text = 'moviehash'
  movieHashMemberVal = ET.SubElement(movieHashMember, 'value')
  movieHashMemberValString = ET.SubElement(movieHashMemberVal, 'string')
  movieHashMemberValString.text = testHash
  
  movieByteMember = ET.SubElement(struct, 'member')
  movieByteMemberName = ET.SubElement(movieByteMember,  'name')
  movieByteMemberName.text = 'moviebytesize'
  movieByteMemberVal = ET.SubElement(movieByteMember, 'value')
  movieByteMemberValString = ET.SubElement(movieByteMemberVal, 'double')
  movieByteMemberValString.text = str(testSize) 
  xmlStuff = ET.tostring(tree)
  message = '<?xml version="1.0" encoding="utf-8"?>' + str(xmlStuff)
  headers = {"Content-Type" : 'application/xml'}
  #  r=requests.post('​http://api.opensubtitles.org/xml-rpc', headers=headers, data = xmlStuff)
  r = requests.post('https://api.opensubtitles.org/xml-rpc', headers=headers, data=xmlStuff)
  result = ET.fromstring(r.text)
  writestream = open('templates/output.html', 'w')
  writestream.write(r.text)
  return render_template('output.html')
  



def xml():
  tree = ET.fromstring('<methodCall></methodCall>')
  name = ET.SubElement(tree, 'methodName')
  name.text = 'LogIn'
  params = ET.SubElement(tree, 'params')
  username = ET.SubElement(params, 'param')
  userVal = ET.SubElement(username, 'value')
  userString = ET.SubElement(userVal, 'string')
  userString.text=''

  password = ET.SubElement(params, 'param')
  passVal = ET.SubElement(password, 'value')
  passString = ET.SubElement(passVal, 'string')
  passString.text=''

  language = ET.SubElement(params, 'param')
  languageVal = ET.SubElement(language, 'value')
  languageString = ET.SubElement(languageVal, 'string')
  languageString.text = 'en'

  userAgent = ET.SubElement(params, 'param')
  agentVal = ET.SubElement(userAgent, 'value')
  agentString = ET.SubElement(agentVal, 'string')
  agentString.text = 'SubSeeker'
  xmlStuff = ET.tostring(tree)
  message = '<?xml version="1.0" encoding="utf-8"?>' + str(xmlStuff)
  print(message)
  headers = {"Content-Type" : 'application/xml'}
  #  r=requests.post('​http://api.opensubtitles.org/xml-rpc', headers=headers, data = xmlStuff)
  r = requests.post('https://api.opensubtitles.org/xml-rpc', headers=headers, data=xmlStuff)
  result = ET.fromstring(r.text)
  return result.findall('./params/param/value/struct/member/value/string')[0].text

xml()
app.run()