from flask_whooshee import *
from passlib.hash import pbkdf2_sha256
from backend import whooshee, application, db
from flask_sqlalchemy import SQLAlchemy
import traceback


class User_movies(db.Model):
    __tablename__ = 'user_movies'
    user_id = db.Column('user_id', db.Integer, db.ForeignKey('users.id'), primary_key=True)
    imdb_ID = db.Column('imdb_ID', db.String(50), db.ForeignKey('movies.imdb_ID'), primary_key=True)
    time_pos = db.Column('time_pos', db.String(10), nullable=True)
    finished = db.Column('finished', db.Boolean)
    users = db.relationship('Users', back_populates='movies')
    movies = db.relationship('Movies', back_populates='users')

    @classmethod
    def add_relation(self, relation):
        db.session.add(relation)
        db.session.commit()

class User_series(db.Model):
    __tablename__ = 'user_series'
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), primary_key=True)
    imdb_ID = db.Column(db.String(50), db.ForeignKey('series.imdb_ID'), primary_key=True)
    last_season = db.Column(db.Integer, nullable=True)
    last_episode = db.Column(db.Integer, nullable=True)
    time_pos = db.Column(db.String(10), nullable=True)
    users = db.relationship('Users', back_populates='series')
    series = db.relationship('Series', back_populates='users')

class Users(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key= True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password_hash = db.Column(db.String(200), nullable=False)
    movies = db.relationship('User_movies', back_populates = 'users')
    series = db.relationship('User_series', back_populates = 'users')

    def __repr__(self):
        return '<User %r>' % self.username
    
    @classmethod
    def add_user(self, user):
        db.session.add(user)
        db.session.commit()

    @classmethod
    def find_by_username(self,username):
        return db.session.query(self).filter_by(username = username).first()

    @classmethod
    def find_by_email(self, email):
        return db.session.query(self).filter_by(email = email).first()

    @staticmethod
    def pass_hash(password):
        return pbkdf2_sha256.hash(password)

    @staticmethod
    def verify_hash(password, password_hash):
        return pbkdf2_sha256.verify(password, password_hash)

class Content(db.Model):
    __tablename__ = 'content'
    id = db.Column(db.Integer, primary_key= True)
    location = db.Column(db.String(200), nullable= False)

class Movies(db.Model):
    __tablename__ = 'movies'
    imdb_ID = db.Column(db.String(50), primary_key=True, nullable=False)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False)
    content = db.relationship('Content',
        backref = db.backref('movies', lazy=True))
    users = db.relationship('User_movies', back_populates = 'movies')
    
    @classmethod
    def add_movie(self, movie):
        db.session.add(movie)
        db.session.commit()


series_seasons = db.Table('series_seasons',
            db.Column('imdb_ID', db.String(50), db.ForeignKey('series.imdb_ID'), primary_key=True),
            db.Column('seasons_id', db.Integer, db.ForeignKey('seasons.id'), primary_key=True))

seasons_episodes = db.Table('seasons_episodes',
            db.Column('seasons_id', db.Integer, db.ForeignKey('seasons.id'), primary_key=True),
            db.Column('episodes_id', db.Integer, db.ForeignKey('episodes.id'), primary_key=True))

class Series(db.Model):
    __tablename__ = 'series'
    imdb_ID = db.Column(db.String(50), primary_key=True, nullable=False)
    users = db.relationship('User_series', back_populates = 'series')


class Seasons(db.Model):
    __tablename__ = 'seasons'
    id = db.Column(db.Integer, primary_key=True)
    season_num = db.Column(db.Integer, nullable=False)
    episode_count = db.Column(db.Integer, nullable=False)

class Episodes(db.Model):
    __tablename__ = 'episodes'
    id = db.Column(db.Integer, primary_key=True)
    imdb_ID = db.Column(db.String(50), nullable=False)
    content_id = db.Column(db.Integer, db.ForeignKey('content.id'), nullable=False)
    content = db.relationship('Content',
        backref = db.backref('episodes', lazy=True))

class RevokedTokenModel(db.Model):
    __tablename__ = 'revoked_tokens'
    id = db.Column(db.Integer, primary_key = True)
    jti = db.Column(db.String(300))

try:
    db.create_all()
except:
    traceback.print_exc()
