from flask import Response, json, render_template, request, send_from_directory, send_file, redirect, jsonify, session
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_raw_jwt, jwt_required, set_access_cookies, unset_jwt_cookies)
from flask_whooshee import *

from backend import (MOVIE_LOCATION, application, database_tables,jwt, db,
                     whooshee)
from torrentparser import TorrentParser
import json
from bs4 import BeautifulSoup
import mechanize
import re
import time
import sys
import _thread
import subprocess
import os
import requests
import traceback

Users = database_tables.Users
Movies = database_tables.Movies
RevokedTokenModel = database_tables.RevokedTokenModel
Content = database_tables.Content
User_movies = database_tables.User_movies
User_series = database_tables.User_series

def find_movie_path(torrent_name, episode):
    print("k")
    raw_torrent = open (torrent_name, 'rb').read()
    torrent_parser = TorrentParser(raw_torrent)
    parsed_torrent = torrent_parser.parse()
    myJson = parsed_torrent
    if "files" in myJson["info"]:
        return myJson["info"]["name"] + "pikachu0101" + myJson["info"]["files"][episode-1]["path"][0]
    else:
        return myJson["info"]["name"]

@application.route('/api/torrent', methods=["POST"])
def get_torrent_file():
    args = request.get_json()
    the_id = args["imdbID"]
    torrent_name = args["title"]
    torrent_type = args["type"]
    br = mechanize.Browser()
    br.set_handle_equiv(True)
    br.set_handle_gzip(True)
    br.set_handle_redirect(True)
    br.set_handle_referer(True)
    br.set_handle_robots(False)
    br.set_handle_refresh(mechanize._http.HTTPRefreshProcessor(), max_time=1)
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    br.open('https://privatehd.to/auth/login')
    br.select_form(nr=0)
    br.form['email_username'] = 'hrothgar32'
    br.form['password'] = 'agh54sdE561Q'
    br.submit()
    if(torrent_type == "movie"):
        k = br.open('https://privatehd.to/movies?imdb=' + the_id)
        g = k.read()
        soup = BeautifulSoup(g.decode('utf-8'), 'lxml')
        tag = soup.find('div', {'class' : 'block'})
        br.open('https://privatehd.to/movies/torrents/' + re.findall('\d+',tag.find('a')['href'])[0] + '?quality=1080p')
        soup = BeautifulSoup(br.response().read().decode('utf-8'), 'lxml')
        tbody = soup.find("tbody")
        myTorrents = tbody.find_all("tr")
        for torrent in myTorrents:
                torrent_badges = torrent.find_all("td")[1].div.find_all("div")[1].find_all("span")
                torrent_checker = torrent.find_all("td")[1].div.find_all("div")[0].a
                print(torrent_checker)
                if len(torrent_badges) == 2:
                    if torrent_badges[1].string != "BluRay Raw" and torrent_checker.string.find("Extras") == -1:
                        br.retrieve(torrent.find_all("td")[2].a["href"], ''.join([torrent_name, '.torrent']))
                        break
        return jsonify({"message": "fuck yeah!"})
    else:
        season = args["season"]    
        k = br.open('https://privatehd.to/tv-shows?imdb=' + the_id)
        g = k.read()
        soup = BeautifulSoup(g.decode('utf-8'), 'lxml')
        tag = soup.find('div', {'class' : 'block'})
        br.open('https://privatehd.to/movies/torrents/' + re.findall('\d+',tag.find('a')['href'])[0] + '?quality=1080p')
        soup = BeautifulSoup(br.response().read().decode('utf-8'), 'lxml')
        tbody = soup.find("tbody")
        myTorrents = tbody.find_all("tr")
        for torrent in myTorrents:
            torrent_badges = torrent.find_all("td")[1].div.find_all("div")[1].find_all("span")
            torrent_checker = torrent.find_all("td")[1].div.find_all("div")[0].a
            print(torrent_checker)
            if len(torrent_badges) == 3:
                if torrent_badges[1].string != "BluRay Raw" and torrent_badges[2].string == season and torrent_checker.string.find("Extras") == -1:
                    br.retrieve(torrent.find_all("td")[2].a["href"], ''.join([torrent_name, '.torrent']))
                    break
        return jsonify({"message": "fuck yeah!"})
    

def leech(file_name, episode=0):
    try:
        ses = lt.session({'listen_interfaces': '0.0.0.0:6881'})
        info = lt.torrent_info(file_name)
        h = ses.add_torrent({'ti': info, 'save_path': '.'})
        if episode != 0:
            h.file_priority(episode-1, 8)
        h.set_sequential_download(True)
        while (not h.is_seed()):
            s = h.status()

        alerts = ses.pop_alerts()
        for a in alerts:
            if a.category() & lt.alert.category_t.error_notification:
                print(a)

        sys.stdout.flush()

        time.sleep(1)

        print(h.name(), 'complete')
    except Exception:
        traceback.print_exc()


@application.route('/api/download', methods=["POST"])
def download_file():
    args = request.get_json()
    file_name = args["title"] + '.torrent'
    episode = 'episode'
    if episode in args:
        e = args["episode"]
    else:
        e = 0
    _thread.start_new_thread(leech, (file_name,int(e)))
    return jsonify({"file_name" : find_movie_path(file_name, int(e))})

@application.route('/api/insertmovie', methods=["POST"])
def insert_movie():
    args = request.get_json()
    imdbID = args["imdbID"]
    location = args["fileName"]
    new_content = Content(
        location = location
    )
    new_movie = Movies(
        imdb_ID = imdbID,
        content = new_content
    )
    Movies.add_movie(new_movie)
    return jsonify({
        "message" : "Movie succesfully added to the database!"
    }), 201

@application.route('/api/movie_user_relation', methods=['GET', 'POST', 'PUT'])
def movie_user_relation():
    if request.method == "GET":
        user_id = request.args.get('user_id')
        user = Users.query.get(user_id)
        movies = user.movies
        for movie in movies:
            print(movie.imdb_ID)
        return jsonify({
            "Message" : "Okey"
        }), 200
    if request.method == "POST":
        args = request.get_json()
        imdb_ID = args["imdbID"]
        user_id = args["userID"]
        time_pos = args["timePos"]
        new_relation = User_movies(
            imdb_ID = imdb_ID,
            user_id = user_id,
            time_pos = time_pos
        )
        User_movies.add_relation(new_relation)
        return jsonify({
            "message" : "Relation estabelished!"
        }), 201


def pagefy(items, page):
    temp_itm = []
    pg_n = len(items)
    k = int(page)
    i = (k-1)*10
    j = k*10
    while i < j and i < pg_n:
        temp_itm.append(items[i])
        i += 1
    return jsonify(temp_itm)

@application.route('/api')
@jwt_required
def index():
    return redirect('/api/browse/1', code = 302)

@application.route('/api/browse/<page>', methods = ['GET', 'POST'])
@jwt_required
def pages(page):
    if request.method == 'POST':
        args = request.form 
        text_search = args['text_search']
        results = Movies.query.whooshee_search(text_search).order_by(Movies.id.desc()).all()
        return render_template('index.html', movies = results, location = MOVIE_LOCATION, page = page)
    else:
        items = Movies.get_data()
        return pagefy(items, page)

@application.route('/api/cors', methods = ['POST'])
def cors():
    args = request.get_json()
    r = requests.get(args['URL'])
    return jsonify(r.json())


@application.route('/api/login', methods=['POST', 'OPTION'])
def login():
    args = request.get_json()
    user = Users.find_by_username(args['username'])
    if not user:
        return jsonify({'login' : False}), 401
    if Users.verify_hash(args['password'], user.password_hash):
        access_token = create_access_token(identity = args['username'])
        resp = jsonify({'login': True,
                        'user_id' : user.id})
        set_access_cookies(resp, access_token)
        return resp, 200
    else:
        return jsonify({'login' : False}), 400

def func(a):
    return '"' + a + '"'

@application.route('/api/register', methods=['POST'])
def register():
    args = request.get_json()
    if Users.find_by_username(args['username']):
            return {'message': 'User {} already exists'.format(args['username'])}, 400

    if Users.find_by_username(args['email']):
        return {'message': 'There is already a user with the email: {}'.format(args['email'])}, 400

    new_user = Users(
        username = args['username'],
        email = args['email'],
        password_hash = Users.pass_hash(args['password']),
    )

    Users.add_user(new_user)
    access_token = create_access_token(identity = args['username'])
    refresh_token = create_refresh_token(identity = args['username'])
    return jsonify({
        'message' : 'User succesfully created',
        'access_token' : access_token,
        'refresh_token' : refresh_token
    }), 201

@application.route('/api/data/<movie_name>.mp4')
def data(movie_name):
        start = request.args.get("start") or 0
        movie_name = movie_name.replace("pikachu0101", "/")
        def generate():
            cmdline =''.join(['ffmpeg -ss ', str(start), ' -i ', MOVIE_LOCATION + func(movie_name), ' -f mp4 -vcodec libx264 -crf 18 -acodec mp3 -async 1 -strict experimental -preset ultrafast -movflags frag_keyframe+empty_moov+faststart pipe:1'])
            print(cmdline)
            proc = subprocess.Popen(cmdline, shell=True, stdout=subprocess.PIPE)
            try:
                f = proc.stdout
                byte = f.read(2048)
                while byte:
                    yield byte
                    byte = f.read(2048)
            finally:
                proc.kill()
        return Response(response=generate(), status=200, mimetype='video/mp4', headers={'Access-Control-Allow-Origin': '*', "Content-Type":"video/ogg","Content-Disposition":"inline","Content-Transfer-Enconding":"binary"})

@application.route('/api/data/<movie_name>.js')
def media_content_js(movie_name):
    movie_name = movie_name.replace("pikachu0101", "/")
    d= MOVIE_LOCATION + movie_name 
    if not os.path.isfile( d ): abort(404)
    cmdline= list()
    cmdline.append( "ffmpeg" )
    cmdline.append( "-i" )
    cmdline.append( d )
    duration= -1
    FNULL = open(os.devnull, 'w')
    proc= subprocess.Popen( cmdline, stderr=subprocess.PIPE, stdout=FNULL )
    try:
        for line in proc.stderr:
            line = str(line)
            line= line.rstrip()
            #Duration: 00:00:45.13, start: 0.000000, bitrate: 302 kb/s
            m = re.search('Duration: (..):(..):(..)', line)
            if m is not None: duration= int(m.group(1)) * 3600 + int(m.group(2)) * 60 + int(m.group(3)) + 1
    finally:
        proc.kill()
    return jsonify(duration=duration)

@application.route('/api/logout')
@jwt_required
def logout():
    jti = get_raw_jwt()['jti']
    try:
        revoked_token = RevokedTokenModel(jti = jti)
        db.session.add(revoked_token)
        db.session.commit()
        resp = jsonify({'logout': True})
        unset_jwt_cookies(resp)
        return resp, 200
    except:
        return jsonify({'logout': False}), 500
