import json
import os
import sys
import datetime

from flask import (Flask, Response, abort, escape, jsonify, make_response,
                   render_template, request, send_from_directory, session)
from flask_jwt_extended import JWTManager
from flask_whooshee import *
from flask_sqlalchemy import *

MOVIE_LOCATION = '/home/hrothgar/Documents/media-server/'
application = Flask(__name__)
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////home/hrothgar32/Documents/media-server/media.db'
application.config['JWT_SECRET_KEY'] = 'super-secret'
application.config['SECRET_KEY'] = 'more-super-secret'
application.config['JWT_TOKEN_LOCATION'] = ['cookies']
application.config['JWT_COOKIE_SECURE'] = False
application.config['JWT_ACCESS_TOKEN_EXPIRES'] = datetime.timedelta(days = 1)
jwt = JWTManager(application)
whooshee = Whooshee(application)
db = SQLAlchemy(application)

@application.after_request
def after_request(response):
  response.headers.add('Access-Control-Allow-Origin', 'http://127.0.0.1:3000')
  response.headers.add('Access-Control-Allow-Headers', 'Content-Type, Accept')
  response.headers.add('Access-Control-Allow-Credentials', 'true')
  return response

from backend import database_tables, views

@jwt.token_in_blacklist_loader
def check_if_token_in_blacklist(decrypted_token):
    jti = decrypted_token['jti']
    return database_tables.RevokedTokenModel.is_jti_blacklisted(jti)
