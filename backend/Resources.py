import json
import os
import random
import string
from base64 import b64decode

import requests
from cv2 import cv2
from flask import (Response, abort, jsonify, render_template,
                   send_from_directory)
from flask_jwt_extended import (create_access_token, create_refresh_token,
                                get_jwt_claims, get_jwt_identity, get_raw_jwt,
                                jwt_refresh_token_required, jwt_required)
from flask_mail import Mail, Message
from flask_restful import Api, Resource, reqparse
from passlib.hash import pbkdf2_sha256

from database_tables import (RevokedTokenModel, Users, Videos, db_session,
                             engine, session)


class UserResource(Resource):
    def post(self): #REGISTER
        data_parser = reqparse.RequestParser()
        data_parser.add_argument('username')
        data_parser.add_argument('password')
        data_parser.add_argument('email')
        args = data_parser.parse_args()

        if Users.find_by_username(args['username']):
            return {'message': 'User {} already exists'.format(args['username'])}, 400

        if Users.find_by_username(args['email']):
            return {'message': 'There is already a user with the email: {}'.format(args['email'])}, 400

        new_user = Users(
            username = args['username'],
            email = args['email'],
            password_hash = Users.pass_hash(args['password']),
            preference_path = None
        )

        new_user.add_user()
        access_token = create_access_token(identity = args['username'])
        refresh_token = create_refresh_token(identity = args['username'])
        return {
            'message' : 'User succesfully created',
            'access_token' : access_token,
            'refresh_token' : refresh_token
            }, 201
    
    @jwt_required
    def put(self):  #CHANGE PASSWORD
        data_parser = reqparse.RequestParser()
        data_parser.add_argument('old_password')
        data_parser.add_argument('new_password')
        args = data_parser.parse_args()
    
        username = get_jwt_identity()
        user = Users.find_by_username(username)

        try:
            if user.password_hash == Users.pass_hash(args['old_password']):
                user.password_hash = Users.pass_hash(args['new_password'])
                user.add_user()
        except:
            return {'massage' : 'Something went wrong'}, 500

    def patch(self):    #FORGOT PASSWORD
        data_parser = reqparse.RequestParser()
        data_parser.add_argument('new_password')
        args = data_parser.parse_args()

        username = get_jwt_identity()
        user = Users.find_by_username(username)

        try:
            user.password_hash = Users.pass_hash(args['new_password'])
            user.add_user()
        except:
            return {'massage' : 'Something went wrong'}, 500

class LoginResource(Resource):
    def post(self):
        data_parser = reqparse.RequestParser()
        data_parser.add_argument('username')
        data_parser.add_argument('password')
        args = data_parser.parse_args()

        user = Users.find_by_username(args['username'])

        if not user:
            return {'message': 'Wrong password or username'}, 400

        if Users.verify_hash(args['password'], user.password_hash):
            access_token = create_access_token(identity = args['username'])
            refresh_token = create_refresh_token(identity = args['username'])
            return {
                'message' : 'User logged in!',
                'access_token' : access_token,
                'refresh_token' : refresh_token
                }, 201
        else:
            return {'message' : 'Wrong password or username'}, 400

class LogoutAccessResource(Resource):
    @jwt_required
    def post(self):
        jti = get_raw_jwt()['jti']
        try:
            revoked_token = RevokedTokenModel(jti = jti)
            revoked_token.add()
            return {'message': 'Access token has been added to blacklist'}, 201
        except:
            return {'message': 'Something went wrong'}, 500
